<?php

namespace Mkjp\Transform\ClassLoader;

use Composer\Autoload\ClassLoader;

use Mkjp\Transform\Transformer\SourceTransformer;


/**
 * Class that transforms code as it is (auto)loaded using the given transformer
 */
class TransformingLoader extends ClassLoader {
    /**
     * The transformer to apply
     *
     * @var \Transform\Transformer\SourceTransformer
     */
    protected $transformer = null;
    
    /**
     * Creates a new loader that applies the given transformer to loaded files
     *
     * @param \Transform\Transformer\SourceTransformer $transformer
     */
    public function __construct(SourceTransformer $transformer) {
        $this->transformer = $transformer;
    }
    
    /**
     * Includes the given file, applying transformations first
     * 
     * @param string $file
     * @return boolean  True on success, false on failure
     */
    public function includeFile($file) {
        // Get the full path to the file
        $fullPath = realpath($file);
        
        // If the file doesn't exist/couldn't be accessed, issue a failure
        if( $fullPath === false ) {
            trigger_error("Failed to include file: $file", E_USER_WARNING);
            return false;
        }
        
        // Apply the transformer to the contents of the file
        $transformedSource = $this->transformer->apply(
            file_get_contents($fullPath),
            new \ArrayObject([ 'originalFile' => $fullPath ])
        );
        
        // Load the transformed source
        // We prepend an 'end of PHP' token since eval starts in PHP mode
        $success = eval('?>' . $transformedSource);
        
        // If the return value of eval is false, that indicates a parse error, in
        // which case the load was not successful
        // All other return values from eval indicate that the code was successfully
        // included
        return $success !== false;
    }
    
    /**
     * {@inheritdoc}
     */
    public function loadClass($class) {
        // Find the file containing the class definition
        $file = $this->findFile($class);
        
        // If we are not responsible for loading the file, return null
        if( $file === false ) return null;
        
        // Otherwise include the file and return true on success, null on failure
        return $this->includeFile($file) ?: null;
    }
}
