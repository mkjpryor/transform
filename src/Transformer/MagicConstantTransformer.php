<?php

namespace Mkjp\Transform\Transformer;


/**
 * Source transformer that inserts constant expressions in place of __FILE__ and
 * __DIR__ based on the original file name
 */
class MagicConstantTransformer implements SourceTransformer {
    /**
     * {@inheritdoc}
     */
    public function apply($source, \ArrayObject $metadata) {
        // If the constants don't appear anywhere in the file, there is nothing to do
        if( strpos($source, '__DIR__') === false &&
            strpos($source, '__FILE__') === false ) {
            return $source;
        }
        
        // Look for the __FILE__ and __DIR__ tokens and replace them with constant
        // expressions
        $fileName = $metadata['originalFile'];
        $dirName = dirname($fileName);
        
        $transformedSource = '';
        foreach( token_get_all($source) as $token ) {
            // For 'most' tokens, $token is a 3 element array of
            // [token id, string value, line number]
            // For some tokens, $token is string (e.g. ';'). In this case, we
            // want the extracted $token and $value to be the same
            list($token, $value) = (array)$token + [1 => $token];
            
            // Check if we have a T_FILE or T_DIR token to replace
            if( $token === T_FILE ) {
                $value = "'" . $fileName . "'";
            }
            elseif ( $token === T_DIR ) {
                $value = "'" . $dirName . "'";
            }
            
            // Append the value to the transformed source
            $transformedSource .= $value;
        }
        
        return $transformedSource;
    }    
}
