<?php

namespace Mkjp\Transform\Transformer;


interface SourceTransformer {
    /**
     * Apply the source transformation to the given source and return the
     * transformed source
     * 
     * Metadata can be injected by source transformers, but is guaranteed to have
     * the key 'originalFile' containing the fully qualified path to the original
     * source file
     * ArrayObject is used rather than a plain array to get pass-by-reference
     * semantics without worrying about passing by reference...
     * 
     * @param string $source
     * @param \ArrayObject $metadata
     * @return string
     */
    public function apply($source, \ArrayObject $metadata);
}