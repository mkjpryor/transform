<?php

namespace Mkjp\Transform\Transformer;


/**
 * Allows several transformers to be pipelined (i.e. results from one passed to
 * the next)
 */
class Pipeline implements SourceTransformer {
    /**
     * The transformers in the pipeline
     *
     * @var \Transform\Transformer\SourceTransformer
     */
    protected $transformers = [];
    
    /**
     * Create a new pipeline that applies the given transformers in succession
     * (i.e. results passed from one transformer to the next)
     * Transformers are applied in the order they are given
     * 
     * @param array $transformers
     */
    public function __construct(array $transformers) {
        $this->transformers = $transformers;
    }
    
    /**
     * {@inheritdoc}
     */
    public function apply($source, \ArrayObject $metadata) {
        // Apply the transformers in the order they are given
        foreach($this->transformers as $transformer) {
            $source = $transformer->apply($source, $metadata);
        }
        
        return $source;
    }    
}
