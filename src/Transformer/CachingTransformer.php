<?php

namespace Mkjp\Transform\Transformer;

use Doctrine\Common\Cache\Cache;


/**
 * Transformer that wraps another transformation and caches its results so that
 * the 'inner' transformation is only run again if the source has changed
 */
class CachingTransformer implements SourceTransformer {
    /**
     * The hash algorithm to use for generating cache keys from source
     */
    const KEY_HASH_ALGO = "sha256";
    
    /**
     * The inner transformer that we will cache results from
     *
     * @var \Transform\Transformer\SourceTransformer
     */
    protected $inner;
    
    /**
     * The cache to use
     *
     * @var \Doctrine\Common\Cache\Cache 
     */
    protected $cache;
    
    /**
     * Creates a new trasformer that caches the results of the given transformer
     * using the given cache
     * 
     * @param \Transform\Transformer\SourceTransformer $inner
     * @param \Doctrine\Common\Cache\Cache $cache
     */
    public function __construct(SourceTransformer $inner, Cache $cache) {
        $this->inner = $inner;
        $this->cache = $cache;
    }
    
    /**
     * {@inheritdoc}
     */    
    public function apply($source, \ArrayObject $metadata) {
        // Hash the original source to get the cache key
        $key = hash(static::KEY_HASH_ALGO, $source);
        
        // If there is an entry in the cache, return that as the transformed source
        if( $this->cache->contains($key) ) {
            return $this->cache->fetch($key);
        }
        
        // Otherwise, perform the transformation, cache the result and return the
        // transformed source
        $transformedSource = $this->inner->apply($source, $metadata);
        $this->cache->save($key, $transformedSource);
        return $transformedSource;
    }    
}