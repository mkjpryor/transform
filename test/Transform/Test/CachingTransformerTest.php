<?php

namespace Transform\Test;


class CachingTransformerTest extends \PHPUnit_Framework_TestCase {
    /***************************************************************************
     ** Tests to show that cache hits are dealt with correctly
     ***************************************************************************/
    
    /**
     * Test that the cached value is returned if there is a cache hit
     */
    public function testReturnsCachedValueOnCacheHit() {
        $source = "Some source";
        $key = hash(\Transform\Transformer\CachingTransformer::KEY_HASH_ALGO, $source);
        
        // Get a cache that contains the key associated with a known value
        // We assume that (hopefully!) the Doctrine classes are sufficiently tested
        // to use in our tests
        $cache = new \Doctrine\Common\Cache\ArrayCache();
        $cache->save($key, "Cached value");
        
        $transformer = new \Transform\Transformer\CachingTransformer(
            // We don't care about the inner transform at the moment
            $this->getMock(\Transform\Transformer\SourceTransformer::class),
            $cache
        );
        
        // Test that the returned value of apply is the cached value
        $this->assertEquals(
            "Cached value", $transformer->apply($source, new \ArrayObject([]))
        );
    }
    
    /**
     * Test that the inner transform is not executed if there is a cache hit
     */
    public function testInnerTransformNotExecutedOnCacheHit() {
        $source = "Some source";
        $key = hash(\Transform\Transformer\CachingTransformer::KEY_HASH_ALGO, $source);
        
        // Get a cache that contains the key associated with a known value
        // We assume that (hopefully!) the Doctrine classes are sufficiently tested
        // to use in our tests
        $cache = new \Doctrine\Common\Cache\ArrayCache();
        $cache->save($key, "Cached value");
        
        // Build an inner transformer that doesn't expect to be called
        $inner = $this->getMock(\Transform\Transformer\SourceTransformer::class);
        $inner->expects($this->never())->method('apply');
        
        $transformer = new \Transform\Transformer\CachingTransformer($inner, $cache);
        
        // Execute the transformer to see if the expectations pass
        $transformer->apply($source, new \ArrayObject([]));
    }
    
    
    /***************************************************************************
     ** Tests to show that cache misses are dealt with correctly
     ***************************************************************************/
    
    /**
     * Test that the inner transformer is called correctly on cache miss and its value
     * returned
     */
    public function testReturnsTransformedValueOnCacheMiss() {
        $source = "Some source";
        $key = hash(\Transform\Transformer\CachingTransformer::KEY_HASH_ALGO, $source);
        
        // Build an inner transformer that expects the correct arguments and
        // returns a known value when called
        $inner = $this->getMock(\Transform\Transformer\SourceTransformer::class);
        $inner->expects($this->once())->method('apply')
              ->with($source, new \ArrayObject(['meta' => 'data']))
              ->will($this->returnValue("Transformed source"));
        
        $transformer = new \Transform\Transformer\CachingTransformer(
            $inner,
            // Use an empty cache so we get a cache miss for the key
            new \Doctrine\Common\Cache\ArrayCache()
        );
        
        // Test that the returned value of apply is the transformed source
        $this->assertEquals(
            "Transformed source",
            $transformer->apply($source, new \ArrayObject(['meta' => 'data']))
        );
    }
    
    /**
     * Test that the value from the inner transform is cached when there is a
     * cache miss for the source
     */
    public function testTransformedValueCachedOnCacheMiss() {
        $source = "Some source";
        $key = hash(\Transform\Transformer\CachingTransformer::KEY_HASH_ALGO, $source);
        
        // Build an inner transformer that returns a known value when called
        $inner = $this->getMock(\Transform\Transformer\SourceTransformer::class);
        $inner->expects($this->any())->method('apply')
              ->will($this->returnValue("Transformed source"));
        
        // Create a cache that we will check for cached values
        $cache = new \Doctrine\Common\Cache\ArrayCache();
        
        $transformer = new \Transform\Transformer\CachingTransformer($inner, $cache);
        
        // Check there is no value for the key in the cache
        $this->assertFalse($cache->contains($key));
        
        // Execute the transformer
        $transformer->apply($source, new \ArrayObject([]));
        
        // Check that the correct value exists in the cache
        $this->assertTrue($cache->contains($key));
        $this->assertEquals("Transformed source", $cache->fetch($key));
    }
}
