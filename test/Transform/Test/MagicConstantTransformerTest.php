<?php

namespace Transform\Test;


class MagicConstantTransformerTest extends \PHPUnit_Framework_TestCase {
    /**
     * Test that source with no magic constants is returned unmodified
     */
    public function testSourceWithNoMagicConstantsUnchanged() {
        $source = <<<EOF
<?php

namespace SomeNamespace;

class SomeClass implements AnInterface {
    public function someMethod() {
        var_dump(func_get_args());
    }
}
EOF;
        
        $transformer = new \Transform\Transformer\MagicConstantTransformer();
        
        $transformedSource = $transformer->apply(
            $source, new \ArrayObject([ 'originalFile' => '/path/to/original/file.php' ]));
        
        $this->assertEquals($source, $transformedSource);
    }
    
    /**
     * Test that the magic constant __FILE__ is correctly replaced with the
     * original file
     */
    public function testFileMagicConstantReplaced() {
        $source = <<<EOF
<?php

namespace SomeNamespace;

class SomeClass implements AnInterface {
    public function someMethod() {
        var_dump(%s);
    }
}
EOF;
        $fileName = '/path/to/original/file.php';
        
        $transformer = new \Transform\Transformer\MagicConstantTransformer();
        
        $transformedSource = $transformer->apply(
            // When we pass the source to the transformer, we insert __FILE__
            // where the placeholder is
            sprintf($source, '__FILE__'),
            new \ArrayObject([ 'originalFile' => $fileName ])
        );
        
        // In the transformed source, we expect to see the quoted filename where
        // the placeholder is
        $this->assertEquals(sprintf($source, "'" . $fileName . "'"), $transformedSource);
    }
    
    /**
     * Test that the magic constant __DIR__ is correctly replaced with the
     * directory containing the original file
     */
    public function testDirMagicConstantReplaced() {
        $source = <<<EOF
<?php

namespace SomeNamespace;

class SomeClass implements AnInterface {
    public function someMethod() {
        var_dump(%s);
    }
}
EOF;
        $dirName = '/path/to/original';
        $fileName = $dirName . '/file.php';
        
        $transformer = new \Transform\Transformer\MagicConstantTransformer();
        
        $transformedSource = $transformer->apply(
            // When we pass the source to the transformer, we insert __DIR__
            // where the placeholder is
            sprintf($source, '__DIR__'),
            new \ArrayObject([ 'originalFile' => $fileName ])
        );
        
        // In the transformed source, we expect to see the quoted directory name where
        // the placeholder is
        $this->assertEquals(sprintf($source, "'" . $dirName . "'"), $transformedSource);
    }
    
    /**
     * Check that multiple replacements can be made in a single source
     */
    public function testMultipleReplacements() {
        $source = <<<EOF
<?php

namespace SomeNamespace;

class SomeClass implements AnInterface {
    public function someMethod() {
        var_dump(%s);
        echo %s;
    }

    public function anotherMethod() {
        return %s;
    }
}
EOF;
        $dirName = '/path/to/original';
        $fileName = $dirName . '/file.php';
        
        $transformer = new \Transform\Transformer\MagicConstantTransformer();
        
        $transformedSource = $transformer->apply(
            // When we pass the source to the transformer, we insert __FILE__ and
            // __DIR__ where the placeholders are
            sprintf($source, '__FILE__', '__DIR__', '__FILE__'),
            new \ArrayObject([ 'originalFile' => $fileName ])
        );
        
        // In the transformed source, we expect to see the quoted directory name
        // and file names where the placeholders are
        $this->assertEquals(
            sprintf(
                $source,
                "'" . $fileName . "'", "'" . $dirName . "'", "'" . $fileName . "'"
            ),
            $transformedSource
        );
    }
}