<?php

namespace Transform\Test;


class PipelineTest extends \PHPUnit_Framework_TestCase {
    /**
     * Tests that transformed source is passed correctly through the pipeline
     */
    public function testPipelinedTransformersCalledInOrder() {
        // Create some transformers to pipeline
        $piped1 = $this->getMock(\Transform\Transformer\SourceTransformer::class);
        $piped2 = $this->getMock(\Transform\Transformer\SourceTransformer::class);
        $piped3 = $this->getMock(\Transform\Transformer\SourceTransformer::class);
        
        // The first in the pipeline should be called with the original source
        // and return a known value
        $piped1->expects($this->once())->method('apply')
               ->with("Some source")
               ->will($this->returnValue("Transformer 1"));
        // The second in the pipeline should be called with the value from the
        // first and return a known value
        $piped2->expects($this->once())->method('apply')
               ->with('Transformer 1')
               ->will($this->returnValue('Transformer 2'));
        // The third in the pipeline should be called with the value from the
        // second and return a known value
        $piped3->expects($this->once())->method('apply')
               ->with('Transformer 2')
               ->will($this->returnValue('Transformer 3'));
        
        // Create the pipeline
        $pipeline = new \Transform\Transformer\Pipeline([$piped1, $piped2, $piped3]);
        
        // Assert that we get the value from the last transformer in the pipeline
        // when we give the original source
        $this->assertEquals(
            "Transformer 3", $pipeline->apply("Some source", new \ArrayObject())
        );
    }
}