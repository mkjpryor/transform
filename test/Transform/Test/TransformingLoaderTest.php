<?php

namespace Transform\Test;

use org\bovigo\vfs\vfsStream;


class TransformingLoaderTest extends \PHPUnit_Framework_TestCase {
    /**
     * Test that including a non existent file issues a PHP warning
     * 
     * @expectedException \PHPUnit_Framework_Error_Warning
     */
    public function testIncludingNonExistentFileIssuesWarning() {
        $loader = new \Transform\ClassLoader\TransformingLoader(
            $this->getMock(\Transform\Transformer\SourceTransformer::class)
        );
        
        $loader->includeFile('non_existent_file.php');
    }
    
    /**
     * Test that including a non existent file returns false (for failure) if
     * errors are supressed (or not converted to exceptions)
     */
    public function testIncludingNonExistentFileReturnsFalse() {
        $loader = new \Transform\ClassLoader\TransformingLoader(
            $this->getMock(\Transform\Transformer\SourceTransformer::class)
        );
        
        // If we include a non-existent file with errors suppressed, we should get
        // false
        $this->assertFalse(@$loader->includeFile('non_existent_file.php'));
    }
    
    /**
     * Test that the transformer is called correctly with the contents of the
     * file to include and the fully qualified name in the metadata
     */
    public function testTransformerCalledWithContentsForIncludedFile() {
        $file = __DIR__ . "/../../files/exists.txt";
        $contents = file_get_contents($file);
        
        $transformer = $this->getMock(\Transform\Transformer\SourceTransformer::class);
        $transformer->expects($this->once())->method('apply')
                    ->with($contents,
                           new \ArrayObject([ 'originalFile' => realpath($file) ]));
        
        $loader = new \Transform\ClassLoader\TransformingLoader($transformer);
        $loader->includeFile($file);
    }
    
    /**
     * Test that includeFile returns false if the transformer returns code that
     * can't be parsed
     */
    public function testIncludeFileReturnsFalseForErroredCode() {
        // Create a mock transformer that just returns some un-parsable code when
        // called
        $transformer = $this->getMock(\Transform\Transformer\SourceTransformer::class);
        $transformer->expects($this->any())->method('apply')
                    ->will($this->returnValue('<?php class return { }'));
        
        $loader = new \Transform\ClassLoader\TransformingLoader($transformer);
        // For this test, it doesn't actually matter which file we include as long
        // as it exists, since the transformer has a fixed return value
        // We suppress the text of the parse error as we don't want to junk the
        // PHPUnit output
        $this->assertFalse(@$loader->includeFile(__DIR__ . "/../../files/exists.txt"));
    }
    
    /**
     * Test that includeFile returns true and that any classes/functions in the
     * transformed code are available after it exits
     */
    public function testIncludedFileSuccessfullyEvalsValidCode() {
        // Create a mock transformer that returns some namespaced code that should
        // result in a new class and a new function existing after
        $code = <<<EOF
<?php

namespace TestNamespace;

class SomeClass { }

function some_function() { }
EOF;
        $transformer = $this->getMock(\Transform\Transformer\SourceTransformer::class);
        $transformer->expects($this->any())->method('apply')
                    ->will($this->returnValue($code));
        
        $loader = new \Transform\ClassLoader\TransformingLoader($transformer);
        
        // Check that the class and function we will load don't already exist
        $this->assertFalse(class_exists('TestNamespace\SomeClass', false));
        $this->assertFalse(function_exists('TestNamespace\some_function'));
        
        // For this test, it doesn't actually matter which file we include as long
        // as it exists, since the transformer has a fixed return value
        // includeFile should return true to indicate success
        $this->assertTrue($loader->includeFile(__DIR__ . "/../../files/exists.txt"));
        
        // The classes in the transformed code should now be defined
        $this->assertTrue(class_exists('TestNamespace\SomeClass', false));
        $this->assertTrue(function_exists('TestNamespace\some_function'));
    }
        
    /**
     * Test that loadClass returns null if the loader is not responsible for
     * loading the given class
     */
    public function testLoadClassReturnsNullIfNotResponsibleForClass() {
        $loader = new \Transform\ClassLoader\TransformingLoader(
            $this->getMock(\Transform\Transformer\SourceTransformer::class)
        );
        
        // Since no class associations have been set on the loader, it is not
        // responsible for loading MyClass
        $this->assertNull($loader->loadClass('MyClass'));
    }
    
    /**
     * Test that loadClass returns true if the file it is told contains the class
     * can be transformed and parsed successfully
     */
    public function testLoadClassReturnsTrueIfClassFileIsValid() {
        // Create a transformer that returns some valid PHP when called
        $transformer = $this->getMock(\Transform\Transformer\SourceTransformer::class);
        $transformer->expects($this->any())->method('apply')
                    ->will($this->returnValue('<?php class SomeClass { }'));
                
        $loader = new \Transform\ClassLoader\TransformingLoader($transformer);
        
        // Associate the class we will ask for with a file
        // For this test, it doesn't actually matter which file it is as long
        // as it exists, since the transformer has a fixed return value
        $loader->addClassMap([ 'MyClass' => __DIR__ . "/../../files/exists.txt" ]);
        
        // We expect loadClass to return true when asked to load the class, because
        // the transformer returns valid PHP
        $this->assertTrue($loader->loadClass('MyClass'));
    }
    
    /**
     * Test that loadClass returns null if the file it is told contains the class
     * cannot be transformed and parsed successfully
     */
    public function testLoadClassReturnsNullIfClassFileIsInvalid() {
        // Create a transformer that returns some invalid PHP when called
        $transformer = $this->getMock(\Transform\Transformer\SourceTransformer::class);
        $transformer->expects($this->any())->method('apply')
                    ->will($this->returnValue('<?php class return { }'));
                
        $loader = new \Transform\ClassLoader\TransformingLoader($transformer);
        
        // Associate the class we will ask for with a file
        // For this test, it doesn't actually matter which file it is as long
        // as it exists, since the transformer has a fixed return value
        $loader->addClassMap([ 'MyClass' => __DIR__ . "/../../files/exists.txt" ]);
        
        // We expect loadClass to return true when asked to load the class, because
        // the transformer returns valid PHP
        // We suppress error messages to avoid cluttering PHPUnit output
        $this->assertNull(@$loader->loadClass('MyClass'));
    }
}